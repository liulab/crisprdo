#!/usr/bin/env python

import os
import sys
import stat
from distutils.core import setup, Extension


def check_settings_file():
    # Do not include lib/settings.py in distribution only
    # lib/settings.py.sample; so if there is no lib/settings.py, quit
    # with some information.
    if os.path.isfile("crisprdo/settings.py"):
        pass
    else:
        sys.stderr.write("CRITICAL: Please copy the crisprdo/settings.py.sample to crisprdo/settings.py, and modify it!")
        sys.exit(1)


def main():
    if float(sys.version[:3]) < 2.7:
        sys.stderr.write("CRITICAL: Python version must be greater than or equal to 2.6! and no more than 3.0!\n")
        sys.exit(1)
    # check_pkg_dependencies()
    check_settings_file()
    setup(name="crisprdo",
          version="1.1",
          description="CRISPR-DO for genome-wide CRISPR design and optimization",
          author='Jian Ma',
          author_email='ma.tongji@gmail.com',
          url='http://cistrome.org/crispr/',
          package_dir={'crisprdo': 'crisprdo'},
          packages=['crisprdo'],
          scripts=['bin/crispr-do'],
          classifiers=[
              'Development Status :: 4 - Beta',
              'Environment :: Console',
              'Intended Audience :: Developers',
              'License :: OSI Approved :: Artistic License',
              'Operating System :: POSIX',
              'Programming Language :: Python',
              'Topic :: Database',
          ])


if __name__ == '__main__':
    main()
