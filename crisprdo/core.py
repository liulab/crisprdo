#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: martin
# @Date:   2018-04-17 09:47:39
# @Last Modified by:   martin
# @Last Modified time: 2018-04-17 16:36:13

import os
import sys
import re
import math
import time
import tempfile

from bx.seq.twobit import TwoBitFile
from optparse import OptionParser

from crisprdo import func
from crisprdo.BwReader import BwIO
from crisprdo.settings import *


pjoin = os.path.join

if DEBUG:
    logger = func.SimpleLogging(level=10)
else:
    logger = func.SimpleLogging(level=20)


def filter_dup(data, columns):
    """filter out dup lines if it is identical in union of columns.

    data: 2-dim matrix
    columns: can be [2,3] use c[2]+c[3]
    """
    out = []
    dup_index = []
    for each in data:
        index = ''.join((str(each[t]) for t in columns))

        if index not in dup_index:
            out.append(each)
            dup_index.append(index)
    return out


def read_chrom_len(filen):
    """Read in a UCSC like chrom len file example: hg19.len

    Return a dict. dic[chrom] = int(length)
    """
    data = func.read_table(filen)
    dic = {}
    for line in data:
        chrom, length = line[:2]
        dic[chrom] = int(length)
    return dic


def reverse(seq):
    """ Return a string of reversed seq.
    """
    return ''.join([refdict[t] for t in seq])[::-1]


def fetch_seq(genome, chrom, start, end, twobit=None):
    """fetch_seq(genome, chrom, start, end, twobit=None)

    This will always return a reference sequence (+).
    Return None if wrong chromosome name, or exceeded start, end position.
    """
    if chrom not in chrom_lib.get(genome):
        #logger.warning('{chrom} not find in chrom lib of {genome}, return empty.'.format(chrom=chrom, genome=genome))
        return ''
    if twobit is None:
        twobit = TwoBitFile(open(STATIC_GENOME_2BIT.format(genome=genome, chrom=chrom), 'rb'))
    twobit_chrom = twobit.get(chrom)
    # read_chrom_len(STATIC_GENOME_CHROM_LEN.format(genome=genome))
    chrom_len = twobit_chrom.size
    if 0 <= start < end < chrom_len:
        seq = twobit_chrom.get(start, end)
        return seq
    else:
        logger.warning(
            'Cannot fetch sequence for {genome} {chrom}:{start}-{end}. Exceeded the boundary, return empty.chrom_len {chrom_len}'.format(**locals()))
        return ''


def get_sgRNA_from_seq_to_file(spacer_len, genome, chrom, seq, outfile, delta_cor=0):
    """Find PAM nGG / nCC on given seq and write 30nt sequences to outfile.
    outfile format, #chrom, start, end, strand, hitseq
    Return outfile fn.
    delta_cor: used to shift the delta_cor while fetch seq from whole genome.
    * the sequence output here are always in 5' -> 3' order, but in general, it should be left to right.
    """
    lasso_seq_length = spacer_len + 10
    twobit = TwoBitFile(open(STATIC_GENOME_2BIT.format(genome=genome, chrom=chrom), 'rb'))
    twobit_chrom = twobit.get(chrom)
    chrom_len_file = STATIC_GENOME_CHROM_LEN.format(genome=genome)
    # read_chrom_len(chrom_len_file) # {'chr19_gl000208_random': 92689, ... }
    chrom_len = twobit_chrom.size
    pattern = re.compile('(?=(G{2}|C{2}))', re.IGNORECASE)
    hits = pattern.finditer(seq)

    count = 0
    outf = open(outfile, 'w')
    outf.write('\t'.join(['#chrom', 'start', 'end', 'hitseq', 'strand']) + '\n')
    # The hitseq is 5' to 3', so if xxxxxxxxxxxxxxxxxxxxNGG in minus strand, the plus should be CCNxxxxxxxxxxxxxxxxxxxx
    for hit in hits:
        hitp = hit.start()
        if seq[hitp].upper() == 'G':
            # I match GG instead of NGG, so here minus 1 more
            start = delta_cor + hitp - spacer_len - 1
            end = delta_cor + hitp + 10 - 1
            strand = '+'
            #if start >=0 and end < chrom_len[chrom] and end > start:
            hitseq = fetch_seq(genome, chrom, start, end, twobit=twobit)  # seq[start:end]
        elif seq[hitp].upper() == 'C':
            start = delta_cor + hitp - 10 + 1 + 2
            end = delta_cor + hitp + spacer_len + 1 + 2
            strand = '-'
            #if start >=0 and end < chrom_len[chrom] and end > start:
            hitseq = reverse(fetch_seq(genome, chrom, start, end, twobit=twobit))
        if hitseq and len(hitseq) == lasso_seq_length and 'N' not in hitseq.upper():
            outf.write('\t'.join([chrom, str(start), str(end), hitseq, strand]) + '\n')
            count += 1
    outf.close()
    logger.info("Get {count} {spacer_len}nt spacer with 3' end for {chrom}. full output length {lasso_seq_length}nt".format(
        chrom=chrom, count=count, spacer_len=spacer_len, lasso_seq_length=lasso_seq_length))
    return outfile


class Designer:
    def __init__(self, spacer_len, genome, chrom, start=0, end=0, lasso_cutoff=-99, job_id=None):
        self.genome = genome  # need
        self.chrom = chrom
        self.start = start
        self.end = end
        self.lasso_cutoff = lasso_cutoff
        self.spacer_len = spacer_len
        self.job_id = job_id

        if self.job_id:
            work_dir = self.job_id
        else:
            work_dir = tempfile.mktemp(dir='.')
        self.job_id = work_dir.split(os.sep)[-1]
        logger.info('Workdir: ' + work_dir)
        logger.info('Job_id: ' + job_id)
        if os.path.exists(work_dir):
            logger.warning('Workdir {0} is not empty.'.format(work_dir))
        else:
            os.mkdir(work_dir)
        self.work_dir = work_dir

    def step_1_scan_PAM_on_seq(self, seq, outfile, seq_strand='+', delta_cor=0):
        """Find PAM nGG / nCC on given seq and write 30nt sequences to outfile.
        outfile format, #chrom, start, end, strand, hitseq
        Return outfile fn.
        delta_cor: used to shift the delta_cor while fetch seq from whole genome.
        * the sequence output here are always in 5' -> 3' order, but in general, it should be left to right.
        """

        chrom = self.chrom
        spacer_len = self.spacer_len
        lasso_seq_length = spacer_len + 10
        if seq_strand == '-':
            seq = reverse(seq)
        # read_chrom_len(chrom_len_file) # {'chr19_gl000208_random': 92689, ... }
        chrom_len = len(seq)
        pattern = re.compile('(?=(G{2}|C{2}))', re.IGNORECASE)
        hits = pattern.finditer(seq)

        count = 0
        scan_out = []
        #outf = open(outfile, 'w')
        # outf.write('\t'.join(['#chrom', 'start', 'end', 'hitseq', 'strand']) + '\n')
        # The hitseq is 5' to 3', so if xxxxxxxxxxxxxxxxxxxxNGG in minus strand, the plus should be CCNxxxxxxxxxxxxxxxxxxxx
        for hit in hits:
            hitp = hit.start()
            # if hitp:
            if seq[hitp].upper() == 'G':
                start = hitp - spacer_len - 1  # I match GG instead of NGG, so here minus 1 more
                end = hitp + 10 - 1
                strand = '+'
            elif seq[hitp].upper() == 'C':
                start = hitp - 10 + 1 + 2
                end = hitp + spacer_len + 1 + 2
                strand = '-'
            if 0 <= start < end < chrom_len:
                hitseq = seq[start:end]
                if strand == '-':
                    hitseq = reverse(hitseq)
                if hitseq and len(hitseq) == lasso_seq_length and 'N' not in hitseq.upper() and 'TTTT' not in hitseq.upper()[:spacer_len]:
                    scan_out.append([chrom, delta_cor + start, delta_cor + end, hitseq, strand])
                    count += 1

        scan_out = filter_dup(scan_out, [0, 1, 2])
        scan_out.sort(key=lambda x: x[1])
        scan_out.insert(0, ['#chrom', 'start', 'end', 'hitseq', 'strand'])
        func.write_table(scan_out, outfile)
        logger.info("Get {count} {spacer_len}nt spacer with 3' end for {chrom}. full length {spacer_len}+10nt".format(**locals()))
        return outfile

    def step_1_scan_PAM_on_region(self, outfile):
        """Find PAM nGG / nCC on given seq and write 30nt sequences to outfile.
        outfile format, #chrom, start, end, strand, hitseq
        Return outfile fn.
        delta_cor: used to shift the delta_cor while fetch seq from whole genome.
        * the sequence output here are always in 5' -> 3' order, but in general, it should be left to right.
        """
        genome, chrom, seq_start, seq_end = self.genome, self.chrom, self.start, self.end
        twobit = TwoBitFile(open(STATIC_GENOME_2BIT.format(genome=genome, chrom=chrom), 'rb'))
        seq = fetch_seq(genome, chrom, seq_start, seq_end, twobit=twobit)
        if len(seq) > 3000:
            logger.info('Fetch seq: {sequence}......'.format(sequence=seq[:2000]))
        else:
            logger.info('Fetch seq: {sequence}'.format(sequence=seq))
        return self.step_1_scan_PAM_on_seq(seq, outfile, delta_cor=seq_start)

    def step_1_scan_PAM_on_region_non_ngg(self, outfile):
        genome, chrom, seq_start, seq_end = self.genome, self.chrom, self.start, self.end
        twobit = TwoBitFile(open(STATIC_GENOME_2BIT.format(genome=genome, chrom=chrom), 'rb'))
        seq = fetch_seq(genome, chrom, seq_start, seq_end, twobit=twobit)
        if len(seq) > 3000:
            logger.info('Fetch seq: {sequence}......'.format(sequence=seq[:3000]))
        else:
            logger.info('Fetch seq: {sequence}'.format(sequence=seq))
        return get_sgRNA_from_seq_to_file_non_ngg(self.spacer_len, self.crispr_system, genome, chrom, seq, outfile, delta_cor=seq_start)
        # return self.step_1_scan_PAM_on_seq_non_ngg(seq, outfile, delta_cor=seq_start)

    def step_1_scan_PAM_on_genome(self):
        """scan PAM from the whole genome.

        It will output one file for each chromosome in the genome, 
        and return a list with all the filename it outputs, each file is a chromosome.
        """
        genome = self.genome
        work_dir = self.work_dir
        logger.info('Step 1: scan_PAM_on_genome')
        start_time = time.time()

        cdir = pjoin(work_dir, 'scan_pam_on_genome')
        if not os.path.exists(cdir):
            os.mkdir(cdir)
        chrom_len_file = STATIC_GENOME_CHROM_LEN.format(genome=genome)
        # {'chr19_gl000208_random': 92689, ... }
        chrom_len = read_chrom_len(chrom_len_file)
        chrom_list = sorted(chrom_lib[genome])

        count = 0
        outf_list = []
        for chrom in chrom_list:
            count += 1
            twobit = TwoBitFile(open(STATIC_GENOME_2BIT.format(genome=genome, chrom=chrom), 'rb'))
            twobit_chrom = twobit.get(chrom)
            seq = twobit_chrom.get(0, twobit_chrom.size)
            outfile = pjoin(cdir, '{genome}.{chrom}.target.list'.format(
                genome=genome, chrom=chrom))
            outf_list.append(outfile)
            logger.debug('Processing {chrom}. {count} / {total}'.format(
                chrom=chrom, count=count, total=len(chrom_list)))
            get_sgRNA_from_seq_to_file(
                self.spacer_len, genome, chrom, seq, outfile, delta_cor=0)

            logger.info('Write scan result to {file}'.format(file=outfile))
        return outf_list

    def step_2_calc_lasso_score(self, filen, outfile, seq_column_index=3):
        """step_2_calc_lasso_score(self, filen, seq_column_index = 3)

        Given a tab delimited file, calc lasso score and append the score to the end column.
        Only output the lines with score > self.lasso_cutoff
        * the seq column index count start 0.
        """
        if filen == outfile:
            logger.error('step 2 calc lasso infile cannot be same as outfile.')
            exit()
        genome = self.genome
        cutoff = self.lasso_cutoff
        work_dir = self.work_dir
        lasso_seq_length = self.spacer_len + 3 + 7

        logger.info('step_2_calc_lasso_score. input {0}'.format(filen))
        start_time = time.time()
        cdir = pjoin(work_dir, 'lasso_score')
        if not os.path.exists(cdir):
            os.mkdir(cdir)

        seqs = (x[seq_column_index] for x in func.read_table(filen))
        seqs = [t.upper() for t in seqs]
        head = seqs.pop(0)

        # generate one-column sequence file for SSC input, and run SSC
        seqf = tempfile.mktemp(dir=work_dir)
        func.write_table(seqs, seqf)
        scoref = tempfile.mktemp(dir=work_dir)
        cmd = '{lasso_bin} -l {lasso_seq_length} -m {lasso_matrix} -i {seqf} -o {scoref}'.format(
            lasso_bin=lasso_bin, lasso_seq_length=lasso_seq_length, lasso_matrix=lasso_matrix.format(**locals()), seqf=seqf, scoref=scoref)
        logger.debug(cmd)
        os.system(cmd)
        logger.debug('step2: Generate lasso score file.')

        inf = open(filen)
        head = inf.readline().strip() + '\tefficiency_score\n'
        outf = open(outfile, 'w')
        outf.write(head)

        scores = list(func.read_table(scoref))
        i = 0
        for line in inf:
            line2 = line.strip().split('\t')
            score = float(scores[i][1])
            if score > cutoff:
                line2.append(scores[i][1])
                outf.write('\t'.join(line2) + '\n')
            i += 1
        inf.close()
        outf.close()
        if not DEBUG:
            os.remove(scoref)
            os.remove(seqf)

    def step_3_calc_off_target_score_bwa(self, filen, outfile, exclude_perfect_match_off_target=False):
        """Using formle at http://crispr.mit.edu/about
        But I also consider the repeats in genome. So the hits might be more than that tool.

        exclude_perfect_match_off_target: set sgRNA score to 0 as long as there are perfect match off-target hits.
        """
        OUTPUT_BWA_MISMATCH_DETAIL = False
        genome = self.genome
        spacer_len = self.spacer_len
        work_dir = self.work_dir
        #consider_offtarget_inDHS = False
        ONLY_OUTPUT_inDHS_sgRNA = False
        M = [0, 0, 0.014, 0, 0, 0.395, 0.317, 0, 0.389, 0.079, 0.445,
             0.508, 0.613, 0.851, 0.731, 0.828, 0.615, 0.804, 0.685, 0.583]

        def score_offtarget_site(pos, d, nmm, len):
            # 0-base offset, len:length of this spacer
            ret = 1.0 / (1 + 4 * (1 - d * 1.0 / (len - 1))) * 1.0 / (nmm**2)
            for k in (1 - M[t - len] for t in pos):
                ret = ret * k
            return ret

        def score_sgrna_offtarget_effect(hits):
            return 1.0 / (1 + sum(hits)) * 100
        # perfect match >=9, score <=10
        # match with one base mismatch. If pos >= 10, single_hit >= 0.15. So if number > 60, score <= 10

        twobit = TwoBitFile(open(STATIC_GENOME_2BIT.format(genome=genome), 'rb'))
        chrom_len = read_chrom_len(STATIC_GENOME_CHROM_LEN.format(genome=genome))

        cdir = pjoin(work_dir, 'off_target')
        if not os.path.exists(cdir):
            os.mkdir(cdir)

        logger.info('step_3_calc_off_target_score')
        start_time = time.time()

        # write out seq to run bwa
        seq_seed_list = []  # store the seqs from sgRNA scan result, in order
        inf = open(filen)
        bwa_infn = tempfile.mktemp(dir=work_dir)
        tmpinf = open(bwa_infn, 'w')
        for line in inf:
            if not line.strip():
                continue
            if line.startswith('#'):
                # [chrom  start   end     hitseq  strand  efficiency_score]
                head = line.strip().split('\t')
                continue
            line2 = line.strip().split('\t')
            chrom, start, end, seq, strand = line2[:5]
            seq_seed = seq[:spacer_len]
            if strand == '+':
                end = int(start) + spacer_len
            else:
                start = int(end) - spacer_len
            seq_seed_list.append(seq_seed)
            tmpinf.write('>seq_{chrom}_{start}_{end}_{strand}\n{seq_seed}\n'.format(**locals()))
        inf.close()
        tmpinf.close()

        # run bwa
        mid_fn = tempfile.mktemp(dir=work_dir)  # pjoin(cdir, 'bwa.mid')
        cmd = 'bwa aln -o 0 -n 3 -N -t 5 %s %s >%s' % (
            STATIC_BWA_INDEX.format(genome=genome), bwa_infn, mid_fn)
        logger.debug(cmd)
        os.system(cmd)
        aligner_outfn = tempfile.mktemp(
            dir=work_dir)  # pjoin(cdir, 'bwa.out.sam')
        cmd = 'bwa samse -n 8000 %s %s %s >%s' % (
            STATIC_BWA_INDEX.format(genome=genome), mid_fn, bwa_infn, aligner_outfn)
        logger.debug(cmd)
        os.system(cmd)

        # read in bwa result each line for one loop
        # Notes: the genome location index of bwa start from 1.
        sgrna2score = {}
        sgrna2score_offtarget_in_dhsexon = {}
        count = 0
        line_count = 0
        mismatch_index_dict = {}  # kill the 3 mismatch position
        for each_line in open(aligner_outfn):
            if each_line.startswith('@'):
                continue
            line_count += 1
            logger.info('line_count: {}'.format(line_count))

            offtarget_regions = []  # offtarget_regions for each sgRNA with score
            offtarget_regions_score = []
            ##offtarget_regions_in_dhsexon = []
            raw_seq = seq_seed_list.pop(0)
            if 'N' in raw_seq.upper():
                sgrna2score[raw_seq] = 0
                continue
            each_line = each_line.strip().split('\t')
            raw_seq_null, raw_seq_chrom, raw_seq_start, raw_seq_end, raw_seq_strand = each_line[0].split('_')
            raw_seq_start = int(raw_seq_start)
            raw_seq_end = int(raw_seq_end)
            map_to_seq = each_line[9]
            map_to_chrom = each_line[2]
            map_to_start = int(each_line[3])

            # parse tags in bwa sam format, assign sgRNA score to 0 if too many mismatches.
            tags = each_line[11:]
            tag_dict = {}
            for tag in tags:
                _tag, _type, _value = tag.split(':')
                tag_dict[_tag] = _value
            x0_best_hits = tag_dict.get('X0', 0)
            x1_suboptimal_hits = tag_dict.get('X1', 0)
            if int(x0_best_hits) + int(x1_suboptimal_hits) > 6000:
                logger.warning('The number of X0+X1 tag for the read is %s %s. #The specificity score is set to 0.' %
                               (x0_best_hits, x1_suboptimal_hits))
                sgrna2score[map_to_seq] = 0
                # mismatch_index_dict[map_to_seq]
                continue
            if map_to_chrom == '*':
                sgrna2score[map_to_seq] = 0
                continue

            # Test and calc map_to_strand
            map_to_seq_ref = fetch_seq(genome, map_to_chrom, map_to_start - 1, map_to_start + spacer_len - 1, twobit=twobit)
            if raw_seq.upper() == map_to_seq:
                map_to_strand = '+'
            elif reverse(raw_seq.upper()) == map_to_seq:
                map_to_strand = '-'
            else:
                logger.error('Raw_seq is not identify with map_to_seq, nor the reverse one. {raw_seq} {map_to_strand}'.format(**locals()))
                exit()

            xa = tag_dict.get('XA', '')
            if exclude_perfect_match_off_target and 'M,0;' in xa:  # if there are perfect match
                sgrna2score[map_to_seq] = 0
                continue
            # a temp value for strand, but it M + actually. will fix next
            xa += '{chrom},{strand}{start},{spacer_len}M,0;'.format(
                chrom=map_to_chrom, strand=map_to_strand, start=map_to_start, spacer_len=spacer_len)
            matches_tmp = xa.strip(';').split(';')
            matches = []
            for e in matches_tmp:
                echr, epos, _, emismatch = e.split(',')
                # if echr in chrom_lib[genome]:
                matches.append((echr, epos[0], epos[1:], emismatch))
            #matches = re.findall('(chr[\dMXYIVLR]+),([+-])(\d+),\d+M,(\d)', xa)
            # if len(matches) != xa.count(';'):
            #    logger.error('RegExp error while grep chrom info from bwa mismatch result. %d' %len(matches))
            #    exit()
            logger.info('Get %d mismatch positions.' % len(matches))
            if len(matches) == 0:
                logger.warning('No XA tag. This could be no mismatches found. The number of X0+X1 is %s %s' %
                               (x0_best_hits, x1_suboptimal_hits))
            mismatch_num_list = [int(t[3]) for t in matches]
            mismatch_base_count_list = [mismatch_num_list.count(t) for t in range(1, 5)]
            logger.info('Mismatch_base_count_list {0} {1} {2} {3}. (mismatches may not follow-up with NGG/NAG)'.format(*mismatch_base_count_list))
            # if mismatch_base_count_list[0] >= 5 or mismatch_base_count_list[1] >= 30:
            #    logger.info('Skip. Mismatch_base_count_list over the cutoff.')
            #    continue

            #matches_outf = open(pjoin(cdir, raw_seq), 'w')
            # matches is a list with all the hit in genome of the 20nt seq_seed including itself (if have perfect match).
            # filter itself out and get others to calc offtarget score.
            # be careful: if seq_seed in minus strand. The result with - mark will be actually from plus strand.
            offtarget_in_dhs_count = 0
            mismatch_index_detail_list = []
            for match in matches:
                # logger.info(match)
                # hits_seqinfo[0]
                rchrom, rstrand, rstart, mismatch_num = match
                rstart = int(rstart)
                if rchrom == raw_seq_chrom and rstart - 1 == raw_seq_start and rstrand == raw_seq_strand:  # This is the seq_seed
                    logger.debug('This is the seq seed')
                    continue
                if rstrand == '+':
                    offtarget_start = rstart - 1
                    offtarget_end = rstart + spacer_len + 3 - 1
                    offtarget_seq = fetch_seq(
                        genome, rchrom, rstart - 1, rstart + spacer_len + 2, twobit=twobit)
                elif rstrand == '-':
                    offtarget_start = rstart - 4
                    offtarget_end = rstart + spacer_len + 3 - 4
                    offtarget_seq = reverse(fetch_seq(genome, rchrom, rstart - 4, rstart + spacer_len - 1, twobit=twobit))
                # logger.debug(offtarget_seq)

                if offtarget_seq[-2:].upper() not in ('GG', 'AG'):  # this is not an off target
                    continue
                if 'N' in offtarget_seq.upper():
                    continue

                mismatch_dict = {}
                mismatch_index_list = []
                for i, bases in enumerate(zip(raw_seq.upper(), offtarget_seq.upper()[:spacer_len])):
                    read_base, refer_base = bases
                    if read_base != refer_base:
                        mismatch_dict[i] = {'refer': refer_base, 'read': read_base}
                        mismatch_index_list.append(str(i))
                num_mismatch = len(mismatch_index_list)
                if len(mismatch_dict) > 3:
                    logger.error('mismatch dict > 3. This should not happen. raw_seq %s off_seq %s' % (raw_seq, offtarget_seq))
                    exit()
                if not mismatch_dict:
                    score = 1
                    offtarget_regions_score.append(score)
                else:
                    pos = mismatch_dict.keys()
                    nmm = len(pos)
                    if len(pos) == 1:
                        d = spacer_len - 1
                    else:
                        d = (max(pos) - min(pos)) * 1.0 / (nmm - 1)
                    # print(match, 'pos=', pos, 'avg d=', d, 'nmm=', nmm)
                    score = score_offtarget_site(pos, d, nmm, spacer_len)
                    offtarget_regions_score.append(score)
                mismatch_index_detail_list.append('{chrom},{start},{strand},{num_mismatch},{mismatch_index};'.format(
                    chrom=rchrom, start=rstart, strand=rstrand,
                    num_mismatch=len(mismatch_index_list),
                    mismatch_index=':'.join(mismatch_index_list)
                ))
            count += 1
            #logger.info('%d offtarget in DHS_exon region.' %offtarget_in_dhs_count)

            # Get the score list for each sgRNA and calc specificity score
            if not ONLY_OUTPUT_inDHS_sgRNA:
                score_list_for_single_sgRNA = offtarget_regions_score
                sgrna_off_target_effect = score_sgrna_offtarget_effect(score_list_for_single_sgRNA)
                logger.debug('sgrna_off_target_effect %.3f with %d offtarget regions. X0+X1: %s+%s' % (
                    sgrna_off_target_effect, len(score_list_for_single_sgRNA), x0_best_hits, x1_suboptimal_hits))
                sgrna2score[map_to_seq] = sgrna_off_target_effect

                if OUTPUT_BWA_MISMATCH_DETAIL:
                    mismatch_index_dict[map_to_seq] = ''.join(mismatch_index_detail_list)
        # logger.debug(sgrna2score)

        # avoid infile and outfile be the same filename
        tmp_filen = tempfile.mktemp(dir=cdir)
        os.rename(filen, tmp_filen)

        inf = open(tmp_filen)
        outf = open(outfile, 'w')
        head = inf.readline()  # 'chrom\tstart\tend\tstrand\tseq\n' #
        outf.write(head.strip('\n') + '\tspecificity_score\n')
        for line in inf:
            seq20nt = line.split()[3][:spacer_len].upper()
            if 'N' in seq20nt:
                continue
            #strand = line.split()[4]
            if not ONLY_OUTPUT_inDHS_sgRNA:
                try:
                    sgrna_score = sgrna2score[seq20nt]
                    if OUTPUT_BWA_MISMATCH_DETAIL:
                        mismatch_detail = mismatch_index_dict[seq20nt]
                except KeyError:
                    sgrna_score = sgrna2score[reverse(seq20nt)]
                    if OUTPUT_BWA_MISMATCH_DETAIL:
                        mismatch_detail = mismatch_index_dict[reverse(seq20nt)]
                if OUTPUT_BWA_MISMATCH_DETAIL:
                    outf.write(line.strip('\n') + '\t%.2f' % sgrna_score + '\t%s\n' % mismatch_detail)
                else:
                    outf.write(line.strip('\n') + '\t%.2f\n' % sgrna_score)

        inf.close()
        outf.close()
        logger.debug(outfile)
        os.remove(tmp_filen)
        if not DEBUG:
            os.remove(aligner_outfn)
            os.remove(mid_fn)

    def step_3_calc_off_target_score(self, filen, outfile, exclude_perfect_match_off_target=False):
        '''A wrapper
        '''
        self.step_3_calc_off_target_score_bwa(filen, outfile, exclude_perfect_match_off_target=False)
        #self.step_3_calc_off_target_score_bowtie(filen, exclude_perfect_match_off_target=False)
        logger.info('Step 3 bwa done.')

    def step_123_from_pre_db(self, pre_calc_sgrna_table, outfile):
        head = '#chrom\tstart\tend\thitseq\tstrand\tefficiency_score\tspecificity_score\n'
        cmd = 'tabix {db} {chrom}:{start}-{end}'.format(
            db=pre_calc_sgrna_table,
            chrom=self.chrom,
            start=self.start,
            end=self.end,
            out_file=outfile,
        )
        data = os.popen(cmd).read()
        data = head + data
        outf = open(outfile, 'w')
        outf.write(data)
        outf.close()

    def check_DHS_overlap_sgRNA(self, filen, outfile):
        genome = self.genome
        work_dir = self.work_dir
        data = func.read_table(filen)
        head = next(data)
        data = list(data)
        head.append('overlap_with_DHS')
        dhs_overlap_work_dir = pjoin(work_dir, 'check_DHS_overlap_sgRNA')
        if not os.path.exists(dhs_overlap_work_dir):
            os.mkdir(dhs_overlap_work_dir)

        for region in data:
            chrom, start, end = region[:3]
            output_overlapped_dhs_fn = pjoin(
                dhs_overlap_work_dir, '{chrom}.{start}.bed'.format(**locals()))
            cmd = 'tabix {dhs_index} {chrom}:{start}-{end} > {output_overlapped_dhs_fn}'.format(
                dhs_index=STATIC_DHS.format(genome=genome), **locals())
            os.system(cmd)
            overlapped_dhs = list(func.read_table(output_overlapped_dhs_fn))
            if overlapped_dhs:
                region.append('True')
            else:
                region.append('False')
        data.insert(0, head)
        func.write_table(data, outfile)

    def check_exon_overlap_sgRNA(self, filen, outfile):
        genome = self.genome
        work_dir = self.work_dir
        data = func.read_table(filen)
        head = next(data)
        data = list(data)
        exon_overlap_work_dir = pjoin(work_dir, 'check_exon_overlap_sgRNA')
        if not os.path.exists(exon_overlap_work_dir):
            os.mkdir(exon_overlap_work_dir)
        for region in data:
            chrom, start, end = region[:3]
            output_overlapped_exon_fn = pjoin(
                exon_overlap_work_dir, '{chrom}.{start}.bed'.format(**locals()))
            cmd = 'tabix {exon_index} {chrom}:{start}-{end} > {output_overlapped_exon_fn}'.format(
                exon_index=STATIC_EXON.format(genome=genome, chrom=chrom), **locals())
            os.system(cmd)
            overlapped_exon = list(func.read_table(output_overlapped_exon_fn))
            if overlapped_exon:
                #harmmock_detail = [t[3] for t in overlapped_exon]
                #symbols = [re.findall('name:"(.[^"]*)"', t) for t in harmmock_detail]
                #symbols = reduce(lambda x, y: x+y, symbols)
                symbols = [t[3] for t in overlapped_exon]
                symbols = list(set(symbols))
                symbols_str = ' '.join(set(symbols))
                region.append(symbols_str)
            else:
                region.append('False')
        head.append('overlap_with_exon')
        data.insert(0, head)
        func.write_table(data, outfile)

    def check_SNP_overlap_sgRNA(self, filen, outfile):
        genome = self.genome
        work_dir = self.work_dir
        data = func.read_table(filen)
        head = next(data)
        data = list(data)
        snp_overlap_work_dir = pjoin(work_dir, 'check_SNP_overlap_sgRNA')
        if not os.path.exists(snp_overlap_work_dir):
            os.mkdir(snp_overlap_work_dir)
        for region in data:
            chrom, start, end = region[:3]
            output_overlapped_snp_fn = pjoin(
                snp_overlap_work_dir, '{chrom}.{start}.bed'.format(**locals()))
            cmd = 'tabix {snp_index} {chrom}:{start}-{end} > {output_overlapped_snp_fn}'.format(
                snp_index=STATIC_SNP.format(genome=genome, chrom=chrom), **locals())
            os.system(cmd)
            overlapped_dhs = list(func.read_table(output_overlapped_snp_fn))
            if overlapped_dhs:
                snps = [t[3] for t in overlapped_dhs]
                snps_str = ' '.join(set(snps))
                region.append(snps_str)
                # region.append('True')
            else:
                region.append('False')
        head.append('overlap_with_SNP')
        data.insert(0, head)
        func.write_table(data, outfile)

    def calc_conservation(self, filen, outfile):
        genome = self.genome
        work_dir = self.work_dir
        data = func.read_table(filen)
        head = next(data)
        data = list(data)
        if data:
            # since the sgrnas all come from one chromosome, this is ok
            chrom = data[0][0]
            bw = BwIO(STATIC_PHASTCONS.format(**locals()))

            for region in data:
                chrom, start, end = region[:3]
                summarize = bw.summarize(
                    chrom, start, end, int(end) - int(start))
                if not summarize:
                    avg_phastcons = '-'
                    region.append(avg_phastcons)
                else:
                    summarize = [t for t in summarize if not math.isnan(t)]
                    if summarize:
                        avg_phastcons = sum(summarize) / len(summarize)
                    else:
                        avg_phastcons = 0
                    region.append(round(avg_phastcons, 4))
        head.append('conservation_score')
        data.insert(0, head)
        func.write_table(data, outfile)


# , offtarget_cutoff=0):
def design_sgrna(spacer_len, genome, chrom, start, end, annotation, lasso_cutoff, job_id=None, seq=None):
    """The offtarget_cutoff is not used not.
    """
    p = Designer(spacer_len, genome, chrom, start, end, lasso_cutoff, job_id)
    work_dir = p.work_dir
    resultfn = pjoin(work_dir, 'spacers')
    pre_calc_sgrna_table = STATIC_PRE_CALC_SGRNA_TABLE.format(**locals())
    logger.debug(resultfn)

    tstart = time.time()
    if seq:
        p.chrom = 'NA'
        p.step_1_scan_PAM_on_seq(seq, outfile=resultfn)
    else:
        if not os.path.exists(pre_calc_sgrna_table):
            p.step_1_scan_PAM_on_region(outfile=resultfn)
    logger.debug('get_sgRNA_from_seq_to_file take {second}s'.format(
        second=time.time() - tstart))

    if os.path.exists(pre_calc_sgrna_table):
        p.step_123_from_pre_db(
            pre_calc_sgrna_table=pre_calc_sgrna_table, outfile=resultfn)
    else:
        p.step_2_calc_lasso_score(resultfn, outfile=resultfn + '.2.tmp')
        tstart = time.time()
        p.step_3_calc_off_target_score(
            resultfn + '.2.tmp', outfile=resultfn, exclude_perfect_match_off_target=False)
    logger.debug('step_2 take {second}s'.format(second=time.time() - tstart))

    if annotation:
        p.check_DHS_overlap_sgRNA(resultfn, outfile=resultfn)
        p.check_SNP_overlap_sgRNA(resultfn, outfile=resultfn)
        p.check_exon_overlap_sgRNA(resultfn, outfile=resultfn)
    #logger.debug('check overlap function take {second}s'.format(second=time.time()-tstart))
    #tstart = time.time()
    logger.info('Finished! result at {0}'.format(resultfn))
    logger.info('Workdir: ' + work_dir)

    return resultfn
