# -*- coding: utf-8 -*-
# @Author: martin
# @Date:   2011-07-22 13:45:10
# @Last Modified by:   martin
# @Last Modified time: 2018-04-17 16:30:19

from __future__ import print_function
import time
import os
import io
import sys
import random
import math
import types
import logging
import traceback
try:
    from pygments.lexers import PythonLexer
    from pygments import highlight
    from pygments.formatters import Terminal256Formatter
except ImportError:
    pass


PY3 = sys.version_info.major == 3
if not PY3:
    range = xrange


def color_print(color_code='', *args, **kwargs):
    isatty = getattr(sys.stderr, 'isatty', lambda: False)()
    if isatty and color_code:
        if os.name == 'nt':
            import ctypes
            SetConsoleTextAttribute = ctypes.windll.kernel32.SetConsoleTextAttribute
            GetStdHandle = ctypes.windll.kernel32.GetStdHandle
            #color_dict = {'red': 0x0C, 'yellow': '0x06', 'green': 0x02}
            #color_code = colro_dict[color]
            color_code_reset = 0x07
            SetConsoleTextAttribute(GetStdHandle(-11), color_code)()
            #sys.stderr.write
            print(value)
            SetConsoleTextAttribute(GetStdHandle(-11), color_code_reset)()
        elif os.name == 'posix':
            #COLOR_RED = '\033[0;31m', COLOR_BOLD_RED = '\033[1;31m'
            #color_code = colro_dict[color]
            color_code_reset = '\033[0m'
            print(color_code, end='')
            print(*args, **kwargs)
            print(color_code_reset, end='')
    else:
        print(value)


class SimpleLogging(object):
    CRITICAL = 50
    # FATAL = CRITICAL
    ERROR = 40
    WARNING = 30
    WARN = WARNING
    SUMMARY = 25
    INFO = 20
    SLEEP = INFO
    DEBUG = 10
    NOTSET = 0

    def __init__(self, *args, **kwargs):
        """available options for init:
        stream, filename, level, levelf, format, datafmt
        """
        # Set options
        self.stream = kwargs.get('stream', sys.stderr)
        self.logfilen = kwargs.get('filename', None)
        if self.logfilen:
            self.logfile = open(self.logfilen, 'ab')
        else:
            self.logfile = None
        self.level = kwargs.get('level', 20)
        self.levelf = kwargs.get('levelf', 25)
        self.format = kwargs.get('format', '[%(asctime)s] %(levelname)-7s - %(message)s\n')
        self.datafmt = kwargs.get('datafmt', '%Y-%m-%d %H:%M:%S')
        self.sys_encode = sys.getfilesystemencoding()
        # Set colors
        self.__write = __write = sys.stderr.write
        self.set_error_color = lambda: None
        self.set_warning_color = lambda: None
        self.set_warn_color = lambda: None
        self.set_summary_color = lambda: None
        self.set_info_color = lambda: None
        self.set_sleep_color = lambda: None
        self.set_debug_color = lambda: None
        self.reset_color = lambda: None
        self.isatty = getattr(sys.stderr, 'isatty', lambda: False)()
        if self.isatty:
            if os.name == 'nt':
                import ctypes
                SetConsoleTextAttribute = ctypes.windll.kernel32.SetConsoleTextAttribute
                GetStdHandle = ctypes.windll.kernel32.GetStdHandle
                self.set_error_color = lambda: SetConsoleTextAttribute(GetStdHandle(-11), 0x0C)
                self.set_warning_color = lambda: SetConsoleTextAttribute(GetStdHandle(-11), 0x06)
                self.set_warn_color = lambda: SetConsoleTextAttribute(GetStdHandle(-11), 0x06)
                self.set_debug_color = lambda: SetConsoleTextAttribute(GetStdHandle(-11), 0x02)
                self.set_sleep_color = lambda: SetConsoleTextAttribute(GetStdHandle(-11), 0x08)
                self.set_debug_color = lambda: SetConsoleTextAttribute(GetStdHandle(-11), 0x0F)
                self.reset_color = lambda: SetConsoleTextAttribute(GetStdHandle(-11), 0x07)
                self.set_info_color = lambda: SetConsoleTextAttribute(GetStdHandle(-11), 0x07)
                self.set_summary_color = lambda: SetConsoleTextAttribute(GetStdHandle(-11), 0x07)
            elif os.name == 'posix':
                self.set_error_color = lambda: __write('\033[31m')
                self.set_warning_color = lambda: __write('\033[33m')
                self.set_warn_color = lambda: __write('\033[33m')
                self.set_debug_color = lambda: __write('\033[32m')
                self.set_sleep_color = lambda: __write('\033[36m')
                self.set_debug_color = lambda: __write('\033[32m')
                self.reset_color = lambda: __write('\033[0m')
                self.set_info_color = lambda: __write('\033[0m')
                self.set_summary_color = lambda: __write('\033[0m')

    def log(self, level, msg):
        logstr = self.format % {'asctime': time.strftime(self.datafmt, time.localtime()), 'levelname': level, 'message': msg}
        level_value = getattr(self, level)
        if level_value >= self.level and self.stream is not None:
            getattr(self, 'set_%s_color' % level.lower())()
            self.__write(logstr)
            self.reset_color()
        if level_value >= self.levelf and self.logfile is not None:
            self.logfile.write(logstr.encode('utf-8'))

    def debug(self, msg):
        self.log('DEBUG', msg)

    def info(self, msg):
        self.log('INFO', msg)

    def summary(self, msg):
        self.log('SUMMARY', msg)

    def sleep(self, msg):
        self.log('SLEEP', msg)

    def warning(self, msg):
        self.log('WARNING', msg)

    def warn(self, msg):
        self.warning(msg)

    def error(self, msg):
        self.log('ERROR', msg)

    # def exception(self, msg):
    #    self.error(msg)
    #    traceback.print_exc(file = sys.stderr)

    def critical(self, msg):
        self.log('CRITICAL', msg)


class ColoredFormatter(logging.Formatter):
    def __init__(self, format, datafmt, use_color=True):
        logging.Formatter.__init__(self, format, datafmt)
        self.use_color = use_color
        self.colors = {'ERROR': '\033[31m',
                       'WARNING': '\033[33m',
                       'WARN': '\033[33m',
                       'DEBUG': '\033[32m',
                       'SLEEP': '\033[36m',
                       'INFO': '\033[0m',
                       'SUMMARY': '\033[0m',
                       }
        self.reset_color = '\033[0m'

    def format(self, record):
        levelname = record.levelname
        log_msg = logging.Formatter.format(self, record)
        if self.use_color and levelname in self.colors:
            return self.colors[levelname] + log_msg + self.reset_color
        else:
            return log_msg


def getlogger(stream=sys.stderr, filename=None, level=20, levelf=20,
              format='[%(asctime)s] %(levelname)-7s - %(message)s', datafmt='%Y-%m-%d %H:%M:%S'):
    """getlogger(stream=sys.stderr, filename = None, level=20, levelf = 20, format='[%(asctime)s] %(levelname)s - %(message)s ', datafmt='%Y-%m-%d %H:%M:%S')
    Level:
    CRITICAL 50
    ERROR    40
    WARNING  30
    INFO     20
    DEBUG    10
    NOTSET    0
    """
    # logging.critical, logging.warning, logging.debug, logging.info
    '''reload(logging)
    if filename is None:
        logging.basicConfig(level=level,
                    format=format,
                    datefmt='%Y-%m-%d %H:%M:%S',
                    stream=stream,
                    filemode=filemode
                    )
    else:
        logging.basicConfig(level=level,
                    format=format,
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename=filename,
                    filemode=filemode
                    )
    return logging
    '''
    # create a logger
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    # handler output format
    if os.name == 'posix':
        formatter = ColoredFormatter(format, datafmt, use_color=True)
    else:
        formatter = logging.Formatter(format, datafmt)
    # create a handle to write to stream if need
    if stream:
        sh = logging.StreamHandler()
        sh.setLevel(level)
        sh.setFormatter(formatter)
        logger.addHandler(sh)

    # create a handle to write to file if need
    if filename:
        fh = logging.FileHandler(filename)
        fh.setLevel(levelf)
        fh.setFormatter(logging.Formatter(format, datafmt))
        logger.addHandler(fh)
    return logger


def myexcepthook(type, value, tb):
    '''value = No module named 'pp'
    tb = <traceback object at 0x108c8c808>
    '''
    COLOR_NC = '\033[0m'  # No Color
    COLOR_RED = '\033[0;31m'
    COLOR_BOLD_RED = '\033[1;31m'
    traceback_lines = traceback.format_exception(type, value, tb)
    traceback_lines[-1] = COLOR_BOLD_RED+traceback_lines[-1]+'\n'
    tbtext = ''.join(traceback_lines)
    sys.stderr.write(COLOR_RED + tbtext + COLOR_NC)

sys.excepthook = myexcepthook


def print_format_table():
    '''prints table of formatted text format options
    '''
    c27 = char(27)  # ascii chara 27 is ESC (^[)
    for style in range(8):
        for fg in range(30, 38):
            for bg in range(40, 48):
                format = '{style};{fg};{bg}'.format(**locals())
                print('{c27}[{format}m {format} {c27}[0m', end='')
            print()
        print('\n')


class ColorManager:
    pass
    '''
    To color output using ANSI escapes, use format: CSI n m
        CSI: escape character: ^[[ or ESC[
        n: numbers, joined by semi-colon.
        m: a literal ASCII m, terminates the escape sequence

    for n:
    +~~~~~+~~~~~~~~~~~~~~~~~~+
    |  n  |  effect          |
    +~~~~~+~~~~~~~~~~~~~~~~~~+
    |  0  |  reset           |
    |  1  |  bold            |
    |  2  |  faint*          |
    |  3  |  italic**        |
    |  4  |  underline       |
    |  5  |  slow blink      |
    |  6  |  rapid blink*    |
    |  7  |  inverse         |
    |  8  |  conceal*        |
    |  9  |  strikethrough*  |
    +~~~~~+~~~~~~~~~~~~~~~~~~+
    * not widely supported
    ** not widely supported and sometimes treated as inverse
    21-29: turns off various text effects (less supported than 1-9)
    +~~~~~~+~~~~~~+~~~~~~~~~~~+
    |  fg  |  bg  |  color    |
    +~~~~~~+~~~~~~+~~~~~~~~~~~+
    |  30  |  40  |  black    |
    |  31  |  41  |  red      |
    |  32  |  42  |  green    |
    |  33  |  43  |  yellow   |
    |  34  |  44  |  blue     |
    |  35  |  45  |  magenta  |
    |  36  |  46  |  cyan     |
    |  37  |  47  |  white    |
    |  39  |  49  |  default  |
    +~~~~~~+~~~~~~+~~~~~~~~~~~+
    38: 8- or 24-bit foreground color (see 8/24-bit Color below)
        CSI 38;2;<r>;<g>;<b>m   24bit (r/g/b range from 0 to 255)
        CSI 38;5;<cid>m   256color (cid range from 0 to 255)
    48: 8- or 24-bit background color (see 8/24-bit Color below)
        CSI 48;2;<r>;<g>;<b>m   24bit (r/g/b range from 0 to 255)

    * Reference:
    https://stackoverflow.com/questions/1448858/how-to-color-system-out-println-output
    SGR: http://ascii-table.com/ansi-escape-sequences-vt-100.php
    '''
    TYPES = []
    def __init__(self):
        self.pattern = '\x1b[{n}m'
        pass

    def get_color(self, styles=None, fg=None, bg=None, mode=None):
        '''color can be int(31), str('red'), tuple/list([255, 255, 255]),  str(#ffffff), 0xffffff 
        mode = 16 / 256 / 24 (24bit)
        '''
        p = []
        if styles is not None:
            p.append(self.getn_style(styles))
        if fg is not None:
            p.append(self.getn_fgbg(fg, 'fg', mode))
        if bg is not None:
            p.append(self.getn_fgbg(bg, 'bg', mode))
        return self.pattern.format(n=';'.join(p))

    def getn_style(self, _style):
        if type(_styles) not in (list, tuple):
            _styles = [_styles]
        return self.pattern.format(n=';'.join(_styles))

    def getn_fgbg(self, _code, _type, mode=None):
        '''code is fg/bg code
        type = fg / bg
        '''
        type2id = {'fg': 38, 'bg': 48}
        typeid = type2id[_type]

        if type(_code) == str and _code.startswith('#'):
            _code = int(_code[1:], 16)
            mode = 24
        if mode == 24:
            _code = self._int2rgb(_code)
            n = '{};2;{};{};{}'.format(typeid, *_code)
        elif mode == 256:
            n = '{};5;{}'.format(typeid, _code)
        elif mode == 16:
            n = str(_code)
        return n

    def reset(self):
        return self.pattern.format(n=0)

    def _int2rgb(self, _int):
        r = (_int >> 16) & 255
        g = (_int >> 8) & 255
        b = _int & 255
        return (r, g, b)


def digist_py(pyfile):
    '''Give a summary of what in a python file
    This func didn't consider triple quotation marks, so may cause some problem'''
    inf = open(pyfile)
    out = []
    for line in inf:
        if line.strip().startswith('class') or line.strip().startswith('def'):
            out.append(line)
    return highlight(''.join(out), PythonLexer(), Terminal256Formatter())


def get_source_code(f):
    '''Get highlighted source code of a function
    '''
    import inspect
    text = inspect.getsource(f)
    return highlight(text, PythonLexer(), Terminal256Formatter())


def listAllFiles(folder):
    """listAllFiles(folder) -> ([folderlist], [filelist])
    Get All folders and files under this folder, generally returns relative path
    depand on your input folder path.
    """
    folder_list = []
    file_list = []
    for dirpath, dirnames, filenames in os.walk(folder):
        folder_list += [os.path.join(dirpath, t) for t in dirnames]
        file_list += [os.path.join(dirpath, t) for t in filenames]
    return (folder_list, file_list)


def helpMethod(object, spacing=10, collapse=1):
    """helpMethod(object, spacing=10, collapse=1) -> None
    Print methods and doc strings.
    Takes module, class, list, dictionary, or string.
    """
    methodList = [method for method in dir(object) if callable(getattr(object, method))]
    processFunc = collapse and (lambda s: " ".join(s.split())) or (lambda s: s)
    print("\n".join(["%s %s" % (method.ljust(spacing), processFunc(str(getattr(object, method).__doc__))) for method in methodList]))


# input a file with '\t' separate, return a 2-d list.
def TableFileInput(fileName, sep="\t", skip='\x00', process_func=None):
    """TableFileInput(fileName, sep="\t", skip="\x00", process_func=None)
    skip the lines start with \x00.
    """
    if type(fileName) == file:
        inf = fileName
    # elif type(fileName) == str: #cause error if filename is unicode
    else:
        try:
            inf = open(fileName)
        except IOError:
            print("no such file: {filename}".format(filename=fileName))
            return None
    l = []
    skipcount = 0
    for line in inf:
        line = line.rstrip("\r\n")
        if line.startswith(skip):
            skipcount += 1
            continue
        if sep == "space":
            line_split = line.split()
        else:
            line_split = line.split(sep)
        if process_func is not None:
            line_split = process_func(line_split)
        l.append(line_split)
    inf.close()
    if skipcount:
        print('Skip {count} lines while reading file.'.format(count=skipcount))
    return l


# file formated dataoutput (list/dict)
def TableFileOutput(l, filename, sep='\t'):
    """TableFileOutput(l,filename,RowNames='F') -> None
    """
    outf = file(filename, 'w')
    if type(l) == list:
        for k in l:
            if type(k) in (list, tuple):
                k = [str(m) for m in k]
                try:
                    outf.writelines(sep.join(k) + '\n')
                except:
                    debug(locals())
            else:
                outf.writelines(str(k) + '\n')
        outf.close()
    elif type(l) == dict:
        for k, v in l.items():
            outf.writelines(k + sep + str(v) + '\n')
        outf.close()
    elif type(l) == tuple:
        pass


def read_table_deco(ins):
    '''Decorator for convert filename to filehandler,
    return None if file not exists
    '''
    def wrapper(table_file, *args, **kwargs):
        if isinstance(table_file, io.TextIOBase):
            inf = table_file
        else:
            try:
                inf = open(table_file)
            except IOError:
                print("no such file: {}".format(table_file))
                return None
        return ins(inf, *args, **kwargs)
    return wrapper


@read_table_deco
def read_table(table_file, sep="\t", skip='\x00', process_func=None):
    """read_table(table_file, sep="\t", skip="\x00", process_func=None)
    skip the lines start with \x00.
    """
    skipcount = 0
    for line in table_file:
        line = line.rstrip("\r\n")
        if line.startswith(skip):
            skipcount += 1
            continue
        if sep == "space":
            line_split = line.split()
        else:
            line_split = line.split(sep)
        if process_func is not None:
            line_split = process_func(line_split)
        yield line_split
    table_file.close()
    if skipcount:
        print('Skip {count} lines while reading file.'.format(count=skipcount))


# file formated dataoutput (list/dict)
def write_table(l, filename, sep='\t'):
    """write_table(l,filename,RowNames='F') -> None
    """
    with open(filename, 'w') as outf:
        if type(l) in (list, tuple, types.GeneratorType):
            for l1 in l:
                if type(l1) in (list, tuple, types.GeneratorType):
                    #writer = csv.DictWriter(outf, delimiter=sep)
                    #writer.writerows(l)
                    k = (str(t) for t in l1)
                    outf.writelines(sep.join(k) + '\n')
                else:
                    outf.write('%s\n' % l1)
        elif type(l) == dict:
            for k in l:
                if type(l[k]) in (list, tuple, types.GeneratorType):
                    outf.write(sep.join([k]+[str(u) for u in l[k]])+'\n')
                else:
                    outf.write('%s%s%s\n' % (k, sep, l[k]))


# Get start lines of a file, output into file <"filename.count">
def get_lines(filename, line_count, startLine=1, rand=False):
    """GetLines(filename, line_count, startLine=1, rand=False)
    get lines of a file
    """
    inf = open(filename)
    for i in range(startLine - 1):
        temp = inf.readline()
    outf = open("%s.%d" % (filename, line_count), "w")
    # count = 0
    if rand:
        lines = inf.readlines()
        length = len(lines)
        print(length)
        for i in range(line_count):
            outf.write(lines[random.randint(0, length)])
    else:
        for i in range(line_count):
            outf.write(inf.readline())
    inf.close()
    outf.close()


# print current time and information on screen
def info(msg, *args, **kwargs):
    print("[%s]" % time.strftime('%Y-%m-%d %H:%M:%S'), msg, *args, **kwargs)


def plist2(m, align='left'):
    if align == 'right':
        tmpl = '{v:>{fill}}'
    else:
        tmpl = '{v:<{fill}}'
    width = [max([len(str(m)) for m in t]) for t in zip(*m)]
    for l in m:
        print(*[tmpl.format(v=t, fill=width[i]+1) for i, t in enumerate(l)])


# screen formated dataoutput
def plist(l, sep="\t"):
    '''plist(l, sep="\t")
    print one line with a element in list.
    You can use index to print a sublist
    '''
    for i in l:
        if type(i) == list:
            print(sep.join([str(t) for t in i]))
        else:
            print(i)


def pdict(d, RowNames='T'):
    """pdict(d,RowNames='T')
    """
    for k, v in d.items():
        print(k + '\t' + str(v))


def listDelDup(l, col=0):
    """listDelDup(l, col = 0)
    """
    length = len(l)
    dic = {}
    for i in l:
        dic[i[col]] = i
    res = dic.values()
    res.sort()
    Info("Delete %d elements" % (length - len(res), ))
    return res


def sort_dict_0(d, order=None):
    """sort_dict_0(d, order=None) -> list of items.
    Sort dict by order, order is a list of keys. Return a list.
    """
    ditems = d.items()
    ditems.sort(key=lambda x: order.index(x[0]))
    return ditems


def binary(num):
    """Return a binary string of a number
    """
    if num == 0:
        return '0'
    return "0b" + "".join([str((num >> i) & 1) for i in range(int(math.floor(math.log(num, 2))), -1, -1)])


def createRefDict(l2, index=0):
    """createRefDict(l2, index=0)
    create a dict using the <index> column as dict's index.
    values are list of the lines.
    """
    out = {}
    for i in l2:
        try:
            out[i[index]].append(i)
        except KeyError:
            out[i[index]] = [i]
    return out


def getVarName(var):
    '''getVarName(var)
    only when call this func directly, it takes effect.
    '''
    import StringIO
    import dis
    # replace standard stdout with a StringIO object
    buf = StringIO.StringIO()
    old_stdout = sys.stdout
    sys.stdout = buf
    # call dis.disco to get bytecodes
    f = sys._getframe(1)
    dis.disco(f.f_code, f.f_lasti)
    # restore standard stdout
    sys.stdout = old_stdout
    # process bytecode
    bytecodes = buf.getvalue().split()
    print(bytecodes)
    i = bytecodes.index("-->")
    var_name = bytecodes[i - 1][1:-1]
    buf.close()
    return var_name


# combinational number, C(x,y)
def C(x, y):
    return math.factorial(x) // math.factorial(y) // math.factorial(x - y)


def hypergeom(k, N, m, n):
    '''hypergeom(k,N,m,n):
    calc pvalue of hypergeom dist.
    k: overlap of A and B.
    N: size of all.
    m: size of A.
    n: size of B.
    '''
    res = 0
    for i in range(0, k + 1):
        aa = C(m, i) * C(N - m, n - i)
        res += aa
    fenmu = C(N, n)
    fenzi = fenmu - res
    pvalue = float(str(fenzi)[:10]) / float(str(fenmu)[:10]) * 10**(len(str(fenzi)) - len(str(fenmu)))
    return pvalue


def intersection(l1, l2, x1=0, x2=0):
    """intersection of 2 lists or 2 2d-lists"""
    if not l1 or not l2:
        return []
    if isinstance(l1[0], list):
        l1 = [t[x1] for t in l1]
    if isinstance(l2[0], list):
        l2 = [t[x2] for t in l2]
    res = set(l1).intersection(set(l2))
    return list(res)


def ll():
    """list files in this folder."""
    l = os.listdir('.')
    l.sort()
    for i in l:
        print('\t' + i)


def debug(var):  # var=locals()
    """debug(var)
    usage: insert debug(locals) into codes.
    """
    import traceback
    while True:
        _ttt = input('> ')
        if _ttt == 'q':
            break
        if not _ttt.startswith('print') and _ttt.find('=') == -1:
            _ttt = 'print ({})'.format(_ttt)
        try:
            exec(_ttt, var)
        except:
            traceback.print_exc()


def selector(_list):
    if len(_list) > 20:
        info("ERROR: no more than 20 items.")
        return
    if len(set(_list)) != len(_list):
        info("ERROR: replicates exists.")
        return
    for i in range(len(_list)):
        print("[%d] %s" % (i+1, _list[i]))
    print("[q] quit")
    while 1:
        k = input("> select an item: ")
        try:
            k = int(k)
        except ValueError:
            pass
        if k == 'q':
            return None
        if 1 <= k <= len(_list):
            return _list[k - 1]


def func_selector(_list2):
    """func_selector(_list2)
    _list2: [(menu, func, args_dict) ... ]
    """
    menus = [t[0] for t in _list2]
    menu = selector(menus)
    if not menu:
        return
    menu, funct, args = [t for t in _list2 if t[0] == menu][0]
    return funct(**args)


def fetchTable(host="localhost", engine="sqlite", username="", passwd="", db="", table="", charset="utf8"):
    """fetchTable(host="localhost", engine="sqlite", username="",passwd="",db="",table="",charset="utf8")
    """
    if engine == "mysql":
        try:
            import MySQLdb
        except ImportError:
            info("MySQLdb not installed.")
            return
        conn = MySQLdb.connect(host=host, user=username, passwd=passwd, db=db, charset=charset)
    elif engine == "sqlite":
        try:
            import sqlite3
        except ImportError:
            info("sqlite3 not installed.")
            return
        conn = sqlite3.connect(database=db)
    cur = conn.cursor()
    if table:
        sql = 'select * from %s;' % table
        cur.execute(sql)
        for each in cur:
            yield each


def getPythonBitVersion():
    """Get the python bit version.
    Return something like: (9223372036854775807, ('64bit', ''))
    """
    import platform
    return sys.maxint, platform.architecture()


def LevenshteinDistance(first, second):
    """Find the Levenshtein distance between two strings."""
    if len(first) > len(second):
        first, second = second, first
    if len(second) == 0:
        return len(first)
    first_length = len(first) + 1
    second_length = len(second) + 1
    distance_matrix = [list(range(second_length)) for x in range(first_length)]
    for i in range(1, first_length):
        for j in range(1, second_length):
            deletion = distance_matrix[i - 1][j] + 1
            insertion = distance_matrix[i][j-1] + 1
            substitution = distance_matrix[i-1][j-1]
            if first[i-1] != second[j - 1]:
                substitution += 1
            distance_matrix[i][j] = min(insertion, deletion, substitution)
    return distance_matrix[first_length-1][second_length-1]


def dice(string):
    """dice('d20+3d6+4')
    """
    string = string.lower()
    flag = '+'
    ts = ''
    data = {'+': [], '-': []}
    for i in string:
        if i == '+':
            data[flag].append(ts)
            ts = ''
            flag = '+'
        elif i == '-':
            data[flag].append(ts)
            ts = ''
            flag = '-'
        else:
            ts += i
    data[flag].append(ts)
    sums = 0
    for i in data['+']:
        if i.find('d') == -1:
            sums += int(i)
        else:
            x, y = i.split('d')
            if not x:
                x = 1
            x = int(x)
            y = int(y)
            sums += sum([random.randint(1, y) for t in range(x)])
    for i in data['-']:
        if i.find('d') == -1:
            sums -= int(i)
        else:
            x, y = i.split('d')
            if not x:
                x = 1
            x = int(x)
            y = int(y)
            sums -= sum([random.randint(1, y) for t in range(x)])
    return sums


def is_number(s):
    """is_number(s)
    check if s is a number. int / float / double.
    """
    try:
        float(s)
        return True
    except ValueError:
        return False


def reverse_seq(seq):
    """reverse_seq(seq)
    reverse a genome sequence with ACGTacgt.
    """
    ref = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A', 'a': 't', 'c': 'g', 'g': 'c', 't': 'a', 'N': 'N'}
    try:
        seq = [ref[t] for t in seq]
    except KeyError:
        seq = []
    return ''.join(seq[::-1])


def split_to_group(s, length):
    """split_to_group(s, length)
    This is not useful but a one line solution.
    input:
    string: s='abcdefghijkl'
    length = 3
    length of s must be multiple of length.
    output a list: ['abc', 'def', 'ghi', 'jkl']
    """
    if len(s) % length != 0:
        return None
    l = [s[t::length] for t in range(length)]
    out = [''.join(k) for k in zip(*l)]
    return out


def mkdirs(name, exist_ok=True):
    """mkdirs(name, exist_ok=True)
    If the target directory already exists, raise an OSError if
    exist_ok is False. Otherwise no exception is raised.
    """
    name = name.rstrip(os.path.sep)
    queue_to_mk = []
    while name and not os.path.exists(name):
        queue_to_mk.insert(0, name)
        name, tail = os.path.split(name)
    if not (queue_to_mk or exist_ok):
        raise os.error('File exists: {}'.format(name))
    else:
        for d in queue_to_mk:
            os.mkdir(d)


def print_dict_tree(dic, indent=0):
    """print_dict_tree(dic, indent=0)
    print complicated dict's key in tree mode.
    """
    for k, v in dic.items():
        print('  ' * indent + str(k))
        if isinstance(v, dict):
            print_dict_tree(v, indent + 1)


def filter_dup(data, columns):
    """filter out dup lines if it is identical in union of columns.
    data: 2-dim matrix
    columns: can be [2,3] use c[2]+c[3]
    """
    out = []
    dup_index = []
    for each in data:
        index = reduce(lambda x, y: x + y, [str(each[t]) for t in columns])
        if index not in dup_index:
            out.append(each)
            dup_index.append(index)
    return out
# historic problem all
Info = info
