# CRISPR-DO
	
## Introduction
The source is the command line version of this website, while it can do the following extra things:

* can do scan for other species if the assembly 2bit and bwa index file is given
* no sequence length limit.

## Install
### Requirement

* 2.6 <= Python < 3
* bx-python
* tabix
* [SSC](http://sourceforge.net/projects/spacerscoringcrispr/)

### Get the source code

	git clone https://github.com/liulab-dfci/crisprdo.git
	
### Download static files

The [2bit file](http://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/) and bwa index (build by yourself) is necessary for run the spacers scan. 
[genome chromosome length](http://cistrome.org/~jian/static_data/chromLen)  
2bit file: 
[hg19](http://hgdownload.soe.ucsc.edu/goldenPath/hg19/bigZips/)
[hg38](http://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/)
[mm9](http://hgdownload.soe.ucsc.edu/goldenPath/mm9/bigZips/)
[mm10](http://hgdownload.soe.ucsc.edu/goldenPath/mm10/bigZips/)  
DHS: 
[hg19](http://cistrome.org/~jian/static_data/crispr/DHS/DHS_hg19.hammock.gz.tbi)
[hg38](http://cistrome.org/~jian/static_data/crispr/DHS/DHS_hg38.hammock.gz.tbi)
[mm9](http://cistrome.org/~jian/static_data/crispr/DHS/DHS_mm9.hammock.gz.tbi)
[mm10](http://cistrome.org/~jian/static_data/crispr/DHS/DHS_mm10.hammock.gz.tbi)  
SNP:
[hg19](http://cistrome.org/~jian/static_data/crispr/SNP/hg19/hg19snp.sort.gz.tbi)
[hg38](http://cistrome.org/~jian/static_data/crispr/SNP/hg38/hg38snp.sort.gz.tbi)
[mm9](http://cistrome.org/~jian/static_data/crispr/SNP/mm9/mm9snp.sort.gz.tbi)
[mm10](http://cistrome.org/~jian/static_data/crispr/SNP/mm10/mm10snp.sort.gz.tbi)
[danRer7](http://cistrome.org/~jian/static_data/crispr/SNP/danRer7/danRer7snp.sort.gz.tbi)
[dm6](http://cistrome.org/~jian/static_data/crispr/SNP/dm6/dm6snp.sort.gz.tbi)
[ce10](http://cistrome.org/~jian/static_data/crispr/SNP/ce10/ce10snp.sort.gz.tbi)  
exon: 
[hg19](http://cistrome.org/~jian/static_data/crispr/exon/exon/hg19.exon.gz)
[hg38](http://cistrome.org/~jian/static_data/crispr/exon/exon/hg38.exon.gz.tbi)
[mm9](http://cistrome.org/~jian/static_data/crispr/exon/exon/mm9.exon.gz.tbi)
[mm10](http://cistrome.org/~jian/static_data/crispr/exon/exon/mm10.exon.gz.tbi)
[danRer7](http://cistrome.org/~jian/static_data/crispr/exon/exon/danRer7.exon.gz.tbi)
[dm6](http://cistrome.org/~jian/static_data/crispr/exon/exon/dm6.exon.gz.tbi)
[ce10](http://cistrome.org/~jian/static_data/crispr/exon/exon/ce10.exon.gz.tbi)


### Config static data before installation

	# Go to the crisprdo directory
	cd crisprdo/crisprdo
	# Config the static files following settings.py.sample
	cp settings.py.sample settings.py
	vim settings.py

### Start to install

	sudo python setup.py install
	
	# Or use this command to install for only you:
	python setup.py install --user
	
If you install locally, you may need to add the location to your $PATH and $PYTHONPATH. These two lines can be added to .bashrc file so it will load each time you login.

	export PYTHONPATH=${HOME}/.local/lib/python2.7/site-packages:${PYTHONPATH}
    export PATH=${HOME}/.local/bin:${PATH}
    
### Test

	crispr-do
	
Usage
	
	Usage: crispr-do <-g genome -c chrX --start=START --end=END> [options]

    sgRNA design for a region.

    Options:
      --version             show program's version number and exit
      -h, --help            Show this help message and exit.
      -g GENOME             Genome assembly version
      -c CHROM, --chrom=CHROM
                            chromosome, e.g.chr1
      --start=START         start site of the region
      --end=END             end site of the region
      --annotation          If to annotation the sgRNAs with DHS, SNP or exons.
                            default (not set): False
      --lasso-cutoff=LASSO_CUTOFF
                            lasso_cutoff
      --job-id=JOB_ID       any string or hash code to identify this run
      
To scan spacers in a region without do annotation, try this

	crispr-do -g GENOME -c CHROM --start=START --end=END --job-is=NAME    
	
To do annotation, you need extra static files, DHS, SNP and exon files.

## Output
The output is called "spacer" in the directory of your JOB_ID. Here's the meaning of each column of that file

column | Description
------------- | -------------
chrom	| the chromosome name the sgRNA located.
start	| start position of the sgRNA in genome
end	| end position of the sgRNA in genome
hitseq	| sgRNA sequence, containing the PAM (in red) and 7bp downstream.
strand	| on which strand the sgRNA located
efficiency_score	| the efficiency of the sgRNA based on its 30bp sequcene.
specificity_score | the specificity of the sgRNA. This score is ranged from 0 to 100
conservation_score | average conservation score in 30bp sequence (20bp guide + PAM + 7bp), using UCSC phastcons score
DHS_overlap | if the sgRNA is overlapped with any DHS regions from encode
SNP overlap | if there are any SNP located in the sgRNA
exon_overlap | if the sgRNA is overlapped with any exon

